const express = require('express')
const app = express()
const port = 3000
const path = require('path')


app.use(express.static("public"))

// app.get('/', (req, res) => {
//   res.send('Hello World!')
// })


app.get('/', function (req, res) {
res.sendFile(path.join(__dirname + '/views/index.html'))
})
app.get('/contato', function (req, res) {
    res.sendFile(path.join(__dirname + '/views/contato.html'))
})
app.get('/sobre', function (req, res) {
res.sendFile(path.join(__dirname + '/views/sobre.html'))
})

app.get('/confirmacao', function (req, res) {
res.send("Obrigado " + req.query.nome + " por ter enviado a mensagem "+ req.query.mensagem + ". Retornaremos no e-mail "+ req.query.email +".");
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})